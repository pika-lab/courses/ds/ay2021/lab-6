package sd.lab.agency.behaviour.impl;

import sd.lab.agency.behaviour.Behaviour;
import sd.lab.agency.Agent;

import java.util.concurrent.CompletableFuture;

public abstract class AwaitPromise<T> implements Behaviour {

    private enum Phase { CREATED, PAUSED, COMPLETED, DONE }

    private Phase phase = Phase.CREATED;
    private CompletableFuture<T> promise;

    @Override
    public void execute(Agent agent) throws Exception {
        if (phase == Phase.CREATED) {
            throw new Error("not implemented");
        } else if (phase == Phase.COMPLETED) {
            throw new Error("not implemented");
        } else {
            throw new IllegalStateException("This should never happen");
        }
    }

    public abstract void onResult(Agent agent, T result) throws Exception;

    // callback to be overridden by subclasses
    public abstract CompletableFuture<T> invokeAsync(Agent agent);

    @Override
    public boolean isPaused() {
        throw new Error("not implemented");
    }

    @Override
    public boolean isOver() {
        throw new Error("not implemented");
    }

    @Override
    public Behaviour deepClone() {
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }
}
