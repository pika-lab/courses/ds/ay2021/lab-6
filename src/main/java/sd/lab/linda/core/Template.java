package sd.lab.linda.core;

public interface Template {
    boolean matches(Tuple tuple);
}
