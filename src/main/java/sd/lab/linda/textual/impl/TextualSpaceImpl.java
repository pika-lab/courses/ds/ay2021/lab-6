package sd.lab.linda.textual.impl;

import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.multiset.HashMultiSet;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;

import javax.print.DocFlavor;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class TextualSpaceImpl implements TextualSpace {

    private final ExecutorService executor;
    private final String name;

    private final MultiSet<StringTuple> tuples = new HashMultiSet<>();
    private final MultiSet<PendingRequest> pendingRequests = new HashMultiSet<>();

    public TextualSpaceImpl(final ExecutorService executor) {
        this(TextualSpace.class.getSimpleName(), executor);
    }

    public TextualSpaceImpl(final String name, final ExecutorService executor) {
        this.name = Objects.requireNonNull(name) + "#" + System.identityHashCode(this);
        this.executor = Objects.requireNonNull(executor);
    }

    @Override
    public CompletableFuture<StringTuple> rd(final RegexTemplate template) {
        log("Requested `rd` operation on template: %s", template);
        final CompletableFuture<StringTuple> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleRd(template, promise));
        return promise;
    }

    @Override
    public CompletableFuture<StringTuple> in(final RegexTemplate template) {
        throw new Error("not implemented");
    }

    @Override
    public CompletableFuture<StringTuple> out(final StringTuple tuple) {
        throw new Error("not implemented");
    }

    @Override
    public CompletableFuture<MultiSet<? extends StringTuple>> get() {
        log("Requested `get` operation");
        final CompletableFuture<MultiSet<? extends StringTuple>> promise = new CompletableFuture<>();
        executor.execute(() -> this.handleGet(promise));
        return promise;
    }

    @Override
    public CompletableFuture<Integer> count() {
        throw new Error("not implemented");
    }

    private synchronized void handleRd(final RegexTemplate template, final CompletableFuture<StringTuple> promise) {
        throw new Error("not implemented");
    }

    private synchronized void handleIn(final RegexTemplate template, final CompletableFuture<StringTuple> promise) {
        throw new Error("not implemented");
    }

    private synchronized void handleOut(final StringTuple tuple, final CompletableFuture<StringTuple> promise) {
        throw new Error("not implemented");
    }

    private synchronized void handleCount(CompletableFuture<Integer> promise) {
        throw new Error("not implemented");
    }

    private synchronized void handleGet(CompletableFuture<MultiSet<? extends StringTuple>> promise) {
        log("Handling `get` operation");
        promise.complete(new HashMultiSet<>(tuples));
    }

    protected void log(String format, Object... args) {
        System.out.printf("[" + getName() + "] " + format + "\n", args);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "TextualSpaceImpl{" +
                ", name='" + name + '\'' +
                ", tuples=" + tuples +
                ", pendingRequests=" + pendingRequests +
                '}';
    }
}
